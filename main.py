# -*- coding: utf8 -*-
import sys

from PySide import QtGui


class Dialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)
        layout = QtGui.QVBoxLayout()
        btn = QtGui.QPushButton()
        btn.setText(u"押して下さい")
        layout.addWidget(btn)
        btn = QtGui.QPushButton()
        btn.setText(u"引いてください")
        layout.addWidget(btn)
        btn = QtGui.QPushButton()
        btn.setText(u"おおおおおおおおおおおお")
        layout.addWidget(btn)


        self.setLayout(layout)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    x = Dialog()
    x.show()
    app.exec_()

